console.log('Hello world!');
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

function fullName() {
    const firstName = txtFirstName.value;
    const lastName = txtLastName.value;
    spanFullName.innerHTML = firstName + ' ' + lastName;
}

txtFirstName.addEventListener('keyup', fullName);
txtLastName.addEventListener('keyup', fullName);
