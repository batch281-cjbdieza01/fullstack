import { Fragment } from 'react';
import CourseCard from '../components/CourseCard';
import coursesData from '../data/coursesData';

export default function Courses(){
	// Check to see if the mock data was captured
	// console.log(coursesData);

	// The "map" method loops through the individual course objects in our array and returns a component for each course
	// Multiple components created through the map method must have a uniqe key that will help React JS indentify which components/elements have been changed
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our courseData array using the courseProp
	const courses = coursesData.map(course => {
		return (
			<CourseCard key = {course.id} courseProp={course} />
		)
	})


	// The course in the CourseCard component is called a prop which is a shorthand for "property" since components are considered as objects in ReactJS
	// The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions rather than hard coded values which use double quotes ""
	// We can pass information fromn one component to another using props. This is refered to as props drilling
	return(
		<Fragment>
			{courses}
		</Fragment>	
	)
}
