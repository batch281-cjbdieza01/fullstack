import { Button, Form, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import PageNotFound from './PageNotFound';

export default function Register() {
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');

  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [isPassed, setIsPassed] = useState(true);
  const [isDisabled, setIsDisabled] = useState(true);
  const [isPasswordMatch, setIsPasswordMatch] = useState(true);

  useEffect(() => {
    if (email.length > 15) {
      setIsPassed(false);
    } else {
      setIsPassed(true);
    }
  }, [email]);

  useEffect(() => {
    if (
      email !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2 &&
      email.length <= 15 &&
      firstName !== '' &&
      lastName !== '' &&
      mobileNumber.length >= 11
    ) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [email, password1, password2, firstName, lastName, mobileNumber]);

  function registerUser(event) {
    event.preventDefault();
    // Check if email already exists before registering
    checkEmailExists(email)
      .then(emailExists => {
        if (emailExists) {
          alert('Email already exists! Please choose a different email.');
        } else {
          // Proceed with registration
          alert('Thank you for registering!');
          navigate('/login');
        }
      })
      .catch(error => {
        console.error('Error checking email existence:', error);
      });
  }

  useEffect(() => {
    if (password1 !== password2) {
      setIsPasswordMatch(false);
    } else {
      setIsPasswordMatch(true);
    }
  }, [password1, password2]);

  // if the email already exists
  function checkEmailExists(email) {
    return fetch('/api/checkEmail', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email }),
    })
      .then(response => response.json())
      .then(data => data.emailExists);
  }

  return (
    user.id === null || user.id === undefined ? (
      <Row>
        <Col className="col-6 mx-auto">
          <h1 className="text-center">Register</h1>
          <Form onSubmit={event => registerUser(event)}>
            <Form.Group className="mb-3" controlId="formBasicFirstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter your first name"
                value={firstName}
                onChange={event => setFirstName(event.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicLastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter your last name"
                value={lastName}
                onChange={event => setLastName(event.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicMobileNumber">
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control
                type="tel"
                placeholder="Enter your mobile number"
                value={mobileNumber}
                onChange={event => setMobileNumber(event.target.value)}
              />
              <Form.Text className="text-muted">
                Mobile number should be at least 11 digits.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={event => setEmail(event.target.value)}
              />
              <Form.Text className="text-danger" hidden={!isPassed}>
                The email should not exceed 15 characters!
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword1">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password1}
                onChange={event => setPassword1(event.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword2">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Retype your nominated password"
                value={password2}
                onChange={event => setPassword2(event.target.value)}
              />
              <Form.Text className="text-danger" hidden={isPasswordMatch}>
                The passwords do not match!
              </Form.Text>
            </Form.Group>

            <Button variant="primary" type="submit" disabled={isDisabled}>
              Sign up
            </Button>
          </Form>
        </Col>
      </Row>
    ) : (
      <PageNotFound />
    )
  );
}
