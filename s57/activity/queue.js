let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array

    let result = [];
    let i = 0;
    while (i < collection.length) {
      result[i] = collection[i];
      i++;
    }
    return result;
  }



function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)

    let index = collection.length;
    collection[index] = element;
    return collection;
  }
    
   


function dequeue() {
    // In here you are going to remove the first element in the array

    if (collection.length === 0) {
        return null;
      }
      const firstElement = collection[0];
      let i = 0;
      while (i < collection.length - 1) {
        collection[i] = collection[i + 1];
        i++;
      }
      collection.length--;
      return firstElement;
    }


function front() {
    // you will get the first element

        if (collection.length === 0) {
          return null;
        }
        return collection[0];
      }
      



function size() {
     // Number of elements   
    
     let count = 0;
     let currentNode = collection;
     while (currentNode[count] !== undefined) {
       count++;
     }
     return count;
   }

function isEmpty() {
    //it will check whether the function is empty or not
    return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
